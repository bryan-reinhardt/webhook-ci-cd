FROM python:3.10.1-alpine3.15

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install Dependencies
RUN set -ex \
    && apk update \
    && apk add --no-cache --virtual .build-deps bash git openssh build-base python3-dev nodejs npm \
    && python -m venv /env \
    && /env/bin/pip install --upgrade pip 

# Change working directory 
WORKDIR /var/www

# Copy requirements
COPY ./requirements.txt ./requirements.txt

# Install requirements
RUN /env/bin/pip install --no-cache-dir wheel \
    && /env/bin/pip install --no-cache-dir -r requirements.txt

# Set environment
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH

COPY . .

EXPOSE 8080

CMD ["gunicorn", "-b", "0.0.0.0:8080", "wsgi:app"]
